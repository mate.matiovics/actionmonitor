FROM openjdk:11
ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} actionmonitor.jar

ENTRYPOINT ["java","-jar","/actionmonitor.jar"]
