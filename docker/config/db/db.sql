-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `actionmonitor` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `actionmonitor`;

-- --------------------------------------------------------

CREATE TABLE `message` (
  `id` bigint(20) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `from_user` varchar(255) DEFAULT NULL,
  `posted_at` datetime DEFAULT NULL,
  `to_user` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

CREATE TABLE `user_dto` (
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `user_dto` (`user_name`, `password`) VALUES
('Gilfoyle', '$2a$10$JIntHJ6C4NDJ9T0sNTKHeOfhaBLdgemrogrxrXrozgpjzytbLURwe'),
('Erlich', '$2a$10$aURp6wpq4.t4i79O.1xpfOWj6XKzbpTO7O1Bn1cMm9OMLLztySt4u');

ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_dto`
  ADD PRIMARY KEY (`user_name`);

ALTER TABLE `message`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
