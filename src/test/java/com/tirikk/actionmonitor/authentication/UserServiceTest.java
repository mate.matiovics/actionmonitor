package com.tirikk.actionmonitor.authentication;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private static final String USER_NAME = "userName";

    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @Test
    void shouldListUsers() {
        List<UserDto> userDtos = List.of(new UserDto(), new UserDto());
        when(userRepository.findAll()).thenReturn(userDtos);

        List<UserDto> returnedUsers = userService.listUsers();

        assertEquals(userDtos, returnedUsers);
    }

    @Test
    void shouldReturnTrueIfUserExists() {
        when(userRepository.existsById(USER_NAME)).thenReturn(true);

        assertTrue(userService.userExists(USER_NAME));
    }

    @Test
    void shouldLoadUserByUsername() {
        UserDto user = new UserDto();
        user.setUserName(USER_NAME);
        when(userRepository.findById(USER_NAME)).thenReturn(Optional.of(user));

        UserDetails userDetails = userService.loadUserByUsername(USER_NAME);

        assertEquals(user.getUserName(), userDetails.getUsername());
    }

    @Test
    void shouldThrowExceptionWhenUserNotFound() {
        when(userRepository.findById(USER_NAME)).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername(USER_NAME));
    }

    @Test
    void shouldRegisterAndLogin() throws ServletException {
        String origPass = "password";
        String encodedPass = "encodedPassword";
        UserDto userDto = new UserDto();
        userDto.setUserName(USER_NAME);
        userDto.setPassword(origPass);

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);

        when(passwordEncoder.encode(origPass)).thenReturn(encodedPass);

        userService.registerAndLogin(mockRequest, userDto);

        assertEquals(encodedPass, userDto.getPassword());
        verify(userRepository).save(userDto);
        verify(mockRequest).login(userDto.getUserName(), origPass);
    }
}
