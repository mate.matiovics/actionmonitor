package com.tirikk.actionmonitor.authentication;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.isA;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AuthenticationController.class)
class AuthenticationControllerTest {
    private static final String LOGIN_PATH = "/auth/login";
    private static final String LOGIN_VIEW = "login";
    private static final String REGISTRATION_PATH = "/auth/register";
    private static final String REGISTRATION_VIEW = "registration";
    private static final String USER_DTO_ATTR = "userDto";
    private static final String USER = "user";
    private static final String PASSWORD = "Password1234";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    void shouldDisplayLoginScreen() throws Exception {
        mockMvc.perform(get(LOGIN_PATH))
                .andExpect(status().isOk())
                .andExpect(view().name(LOGIN_VIEW));
    }

    @Test
    void shouldDisplayRegistrationForm() throws Exception {
        mockMvc.perform(get(REGISTRATION_PATH))
                .andExpect(status().isOk())
                .andExpect(view().name(REGISTRATION_VIEW))
                .andExpect(model().attributeExists(USER_DTO_ATTR))
                .andExpect(model().attribute(USER_DTO_ATTR, isA(UserDto.class)));
    }

    @Test
    void shouldRegisterAndRedirect() throws Exception {
        UserDto userDto = buildUserDto();

        mockMvc.perform(post(REGISTRATION_PATH)
                .with(csrf())
                .flashAttr(USER_DTO_ATTR, userDto))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));

        verify(userService).registerAndLogin(any(HttpServletRequest.class), eq(userDto));
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldRedirectWithError() throws Exception {
        UserDto userDto = buildUserDto();
        doThrow(new ServletException()).when(userService).registerAndLogin(any(HttpServletRequest.class), eq(userDto));

        mockMvc.perform(post(REGISTRATION_PATH)
                .with(csrf())
                .flashAttr(USER_DTO_ATTR, userDto))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(REGISTRATION_PATH + "?error"));
    }

    private UserDto buildUserDto() {
        UserDto userDto = new UserDto();
        userDto.setUserName(USER);
        userDto.setPassword(PASSWORD);
        return userDto;
    }
}
