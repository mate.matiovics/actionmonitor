package com.tirikk.actionmonitor.management;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.messaging.simp.broker.AbstractBrokerMessageHandler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StompBrokerHealthIndicatorTest {

    @Mock
    private AbstractBrokerMessageHandler handler;

    @InjectMocks
    private StompBrokerHealthIndicator indicator;

    @Test
    void shouldReturnUpIfBrokerIsUp() {
        when(handler.isBrokerAvailable()).thenReturn(true);

        Health health = indicator.health();

        assertThat(health.getStatus(), is(Status.UP));
    }

    @Test
    void shouldReturnDownIfBrokerIsDown() {
        when(handler.isBrokerAvailable()).thenReturn(false);

        Health health = indicator.health();

        assertThat(health.getStatus(), is(Status.DOWN));
    }
}
