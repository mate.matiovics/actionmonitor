package com.tirikk.actionmonitor.messaging;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class MessageRepositoryTest {
    private static final String USER_1 = "User1";
    private static final String USER_2 = "User2";
    private static final String USER_3 = "User3";
    private static final String CONTENT = "content";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MessageRepository messageRepository;

    @Test
    void shouldFindAllBetweenUsers() {
        Message message1 = buildMessage(USER_1, USER_2, CONTENT);
        entityManager.persist(message1);

        Message message2 = buildMessage(USER_2, USER_1, CONTENT);
        entityManager.persist(message2);

        Message message3 = buildMessage(USER_1, USER_3, CONTENT);
        entityManager.persist(message3);

        List<Message> messagesBetweenUsers = messageRepository.findAllBetweenUsers(USER_1, USER_2);

        assertThat(messagesBetweenUsers.size(), is(2));
        assertTrue(messagesBetweenUsers.contains(message1));
        assertTrue(messagesBetweenUsers.contains(message2));
    }

    private Message buildMessage(String from, String to, String content) {
        Message message = new Message();
        message.setFromUser(from);
        message.setToUser(to);
        message.setContent(content);
        return message;
    }
}
