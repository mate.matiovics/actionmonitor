package com.tirikk.actionmonitor.messaging;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {
    private static final String DESTINATION = "/queue/message";
    private static final String USER_1 = "user1";
    private static final String USER_2 = "user2";
    private static final String CONTENT = "content";

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private SimpMessagingTemplate messagingTemplate;

    @InjectMocks
    private MessageService messageService;

    @BeforeEach
    void setup() {
        ReflectionTestUtils.setField(messageService, "destination", DESTINATION);
    }

    @Test
    void shouldListMessagesBetweenUsers() {
        Message message1 = new Message();
        message1.setContent(CONTENT);
        Message message2 = new Message();
        message2.setContent(CONTENT.repeat(2));
        List<Message> messages = List.of(message1, message2);
        when(messageRepository.findAllBetweenUsers(USER_1, USER_2)).thenReturn(messages);

        List<Message> returnedMessages = messageService.listMessagesBetween(USER_1, USER_2);

        assertEquals(messages, returnedMessages);
    }

    @Test
    void shouldPersistAndSendMessage() {
        Message message = new Message();
        message.setContent(CONTENT);
        message.setToUser(USER_1);
        when(messageRepository.save(message)).thenReturn(message);

        messageService.persistAndSendMessage(message);

        verify(messageRepository).save(message);
        verify(messagingTemplate).convertAndSendToUser(message.getToUser(), DESTINATION, message);
        verify(messagingTemplate).convertAndSendToUser(message.getFromUser(), DESTINATION, message);
    }
}
