package com.tirikk.actionmonitor.messaging;

import com.tirikk.actionmonitor.authentication.UserDto;
import com.tirikk.actionmonitor.authentication.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.security.Principal;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MessagingController.class)
class MessagingControllerTest {
    private static final String USERS_ATTRIBUTE = "users";
    private static final String MESSAGES_ATTRIBUTE = "messages";
    private static final String CURRENT_USER_ATTRIBUTE = "currentUser";
    private static final String CURRENT_USER = "currentUser";
    private static final String INDEX_VIEW = "index";
    private static final String MESSAGING_VIEW = "messaging";
    private static final String USER = "user";
    private static final String CONTENT = "content";
    private static final String ROOT = "/";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MessagingController messagingController;

    @MockBean
    private UserService userService;

    @MockBean
    private MessageService messageService;

    @Test
    @WithMockUser(username = CURRENT_USER)
    void shouldReturnModelWithListOfUsers() throws Exception {
        List<String> users = List.of("user1", "user2", "user3");
        List<UserDto> dtos = users.stream()
                .map(this::buildUserDto)
                .collect(toList());
        when(userService.listUsers()).thenReturn(dtos);

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name(INDEX_VIEW))
                .andExpect(model().attributeExists(USERS_ATTRIBUTE))
                .andExpect(model().attribute(USERS_ATTRIBUTE, hasSize(3)))
                .andExpect(model().attribute(USERS_ATTRIBUTE, containsInAnyOrder(users.toArray())))
                .andExpect(model().attributeExists(CURRENT_USER_ATTRIBUTE))
                .andExpect(model().attribute(CURRENT_USER_ATTRIBUTE, is(CURRENT_USER)));

        verify(userService).listUsers();
        verifyNoMoreInteractions(userService);
    }

    @Test
    @WithMockUser(username = CURRENT_USER)
    void shouldReturnModelWithMessageHistory() throws Exception {
        List<Message> messages = List.of(new Message(), new Message());
        when(messageService.listMessagesBetween(CURRENT_USER, USER)).thenReturn(messages);
        when(userService.userExists(USER)).thenReturn(true);

        mockMvc.perform(get("/" + USER))
                .andExpect(status().isOk())
                .andExpect(view().name(MESSAGING_VIEW))
                .andExpect(model().attributeExists(CURRENT_USER_ATTRIBUTE))
                .andExpect(model().attribute(CURRENT_USER_ATTRIBUTE, is(CURRENT_USER)))
                .andExpect(model().attributeExists(MESSAGES_ATTRIBUTE))
                .andExpect(model().attribute(MESSAGES_ATTRIBUTE, hasSize(2)))
                .andExpect(model().attribute(MESSAGES_ATTRIBUTE, containsInAnyOrder(messages.toArray())));

        verify(userService).userExists(USER);
        verify(messageService).listMessagesBetween(CURRENT_USER, USER);
        verifyNoMoreInteractions(messageService);
    }

    @Test
    @WithMockUser(username = CURRENT_USER)
    void shouldReturnErrorIfContactDoesNotExist() throws Exception {
        when(userService.userExists(USER)).thenReturn(false);

        mockMvc.perform(get("/" + USER))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(ROOT + "?error"));
    }

    @Test
    void shouldHandleSentMessage() {
        Message message = new Message();
        message.setContent(CONTENT);

        Principal principalMock = mock(Principal.class);
        when(principalMock.getName()).thenReturn(USER);

        messagingController.sendMessage(message, principalMock);

        assertEquals(USER, message.getFromUser());
        verify(messageService).persistAndSendMessage(message);
    }

    private UserDto buildUserDto(String userName) {
        UserDto userDto = new UserDto();
        userDto.setUserName(userName);
        return userDto;
    }
}
