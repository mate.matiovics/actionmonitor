package com.tirikk.actionmonitor.configuration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HttpHandShakeInterceptorTest {
    private static final String SESSION_ID = "sessionId";
    private static final String SESSION_ID_VALUE = "sessionIdValue";

    @InjectMocks
    private HttpHandShakeInterceptor interceptor;

    @Test
    void shouldRegisterSessionID() {
        ServletServerHttpRequest request = mock(ServletServerHttpRequest.class, RETURNS_DEEP_STUBS);
        ServerHttpResponse response = mock(ServerHttpResponse.class);
        WebSocketHandler handler = mock(WebSocketHandler.class);
        Map<String, Object> attributes = new HashMap<>();

        when(request.getServletRequest().getSession().getId()).thenReturn(SESSION_ID_VALUE);

        boolean returnValue = interceptor.beforeHandshake(request, response, handler, attributes);

        assertTrue(returnValue);
        assertTrue(attributes.containsKey(SESSION_ID));
        assertEquals(SESSION_ID_VALUE, attributes.get(SESSION_ID));
    }
}
