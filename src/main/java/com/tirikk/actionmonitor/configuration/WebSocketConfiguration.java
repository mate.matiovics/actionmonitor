package com.tirikk.actionmonitor.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import static com.tirikk.actionmonitor.Constants.QUEUE_PREFIX;
import static com.tirikk.actionmonitor.Constants.TOPIC_PREFIX;

@Configuration
@EnableWebSocketMessageBroker
@RequiredArgsConstructor
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

    @Value("${ws.endpoint}")
    private String websocketEndpoint;

    @Value("${ws.app.destinationPrefix}")
    private String appDestinationPrefix;

    private final BrokerProperties brokerProperties;

    @Override public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(websocketEndpoint)
                .addInterceptors(new HttpHandShakeInterceptor())
                .withSockJS();
    }

    @Override public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableStompBrokerRelay(TOPIC_PREFIX, QUEUE_PREFIX)
                .setRelayHost(brokerProperties.getRelayHost())
                .setRelayPort(brokerProperties.getRelayPort())
                .setSystemLogin(brokerProperties.getSystemLogin())
                .setSystemPasscode(brokerProperties.getSystemPasscode())
                .setClientLogin(brokerProperties.getSystemLogin())
                .setClientPasscode(brokerProperties.getSystemPasscode());
        registry.setApplicationDestinationPrefixes(appDestinationPrefix);
    }
}
