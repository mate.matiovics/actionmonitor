package com.tirikk.actionmonitor.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "broker")
@Data
public class BrokerProperties {
    private String relayHost;
    private int relayPort;
    private String systemLogin;
    private String systemPasscode;
}
