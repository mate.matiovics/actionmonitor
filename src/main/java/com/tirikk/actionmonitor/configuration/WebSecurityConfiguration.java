package com.tirikk.actionmonitor.configuration;

import com.tirikk.actionmonitor.authentication.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.tirikk.actionmonitor.Constants.ACTUATOR_PATH;
import static com.tirikk.actionmonitor.Constants.AUTH_PATH;
import static com.tirikk.actionmonitor.Constants.ERROR_PATH;
import static com.tirikk.actionmonitor.Constants.FULL_LOGIN_PATH;
import static com.tirikk.actionmonitor.Constants.FULL_LOGOUT_PATH;
import static com.tirikk.actionmonitor.Constants.PERFORM_LOGIN;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, @Lazy BCryptPasswordEncoder passwordEncoder, @Lazy UserService userService) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }

    @Override protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(AUTH_PATH + "/**", ERROR_PATH + "**", ACTUATOR_PATH + "/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage(FULL_LOGIN_PATH).loginProcessingUrl(PERFORM_LOGIN).defaultSuccessUrl("/", true)
                .and().logout().logoutUrl(FULL_LOGOUT_PATH).logoutSuccessUrl(FULL_LOGIN_PATH)
                .and().httpBasic();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
