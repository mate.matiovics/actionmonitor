package com.tirikk.actionmonitor.authentication;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    public List<UserDto> listUsers() {
        log.info("Listing users...");
        return userRepository.findAll();
    }

    public boolean userExists(String userName) {
        return userRepository.existsById(userName);
    }

    @Override public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        log.info("Finding user by userName {}", userName);
        UserDto userDto = userRepository.findById(userName).orElseThrow(() -> {
            log.error("User with name {} not found", userName);
            return new UsernameNotFoundException("There is no user with username: " + userName);
        });
        return new AuthUserDetails(userDto);
    }

    public void registerAndLogin(HttpServletRequest request, UserDto userDto) throws ServletException {
        log.info("Registering user with name {}", userDto.getUserName());
        String origPass = userDto.getPassword();

        userDto.setPassword(passwordEncoder.encode(origPass));
        userRepository.save(userDto);

        log.info("Registration of {} successful, logging in...", userDto.getUserName());
        request.login(userDto.getUserName(), origPass);
    }
}
