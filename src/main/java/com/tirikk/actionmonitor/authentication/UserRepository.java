package com.tirikk.actionmonitor.authentication;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserDto, String> {

    @Override
    List<UserDto> findAll();
}
