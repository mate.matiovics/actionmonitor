package com.tirikk.actionmonitor.authentication;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.tirikk.actionmonitor.Constants.AUTH_PATH;
import static com.tirikk.actionmonitor.Constants.FULL_REGISTER_PATH;
import static com.tirikk.actionmonitor.Constants.LOGIN_ENDPOINT;
import static com.tirikk.actionmonitor.Constants.LOGIN_TEMPLATE;
import static com.tirikk.actionmonitor.Constants.REGISTER_ENDPOINT;
import static com.tirikk.actionmonitor.Constants.REGISTRATION_TEMPLATE;
import static java.lang.String.format;

@Controller
@RequiredArgsConstructor
@RequestMapping(AUTH_PATH)
public class AuthenticationController {
    private static final String USER_DTO_ATTR = "userDto";

    private final UserService userService;

    @GetMapping(LOGIN_ENDPOINT)
    public String login() {
        return LOGIN_TEMPLATE;
    }

    @GetMapping(REGISTER_ENDPOINT)
    public String registrationForm(Model model) {
        model.addAttribute(USER_DTO_ATTR, new UserDto());
        return REGISTRATION_TEMPLATE;
    }

    @PostMapping(REGISTER_ENDPOINT)
    public String register(HttpServletRequest request, @ModelAttribute(name = USER_DTO_ATTR) @Valid UserDto userDto) {
        try {
            userService.registerAndLogin(request, userDto);
        } catch (ServletException e) {
            return format("redirect:%s?error", FULL_REGISTER_PATH);
        }
        return "redirect:/";
    }
}
