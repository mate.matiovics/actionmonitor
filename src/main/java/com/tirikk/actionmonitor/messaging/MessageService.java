package com.tirikk.actionmonitor.messaging;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageService {

    @Value("${ws.user.destination}")
    private String destination;

    private final SimpMessagingTemplate messagingTemplate;

    private final MessageRepository messageRepository;

    public List<Message> listMessagesBetween(String user1, String user2) {
        return messageRepository.findAllBetweenUsers(user1, user2);
    }

    public void persistAndSendMessage(Message message) {
        Message savedMessage = messageRepository.save(message);
        log.info("Message from {} saved, sending to recipient...", savedMessage.getFromUser());
        messagingTemplate.convertAndSendToUser(savedMessage.getToUser(), destination, savedMessage);
        messagingTemplate.convertAndSendToUser(savedMessage.getFromUser(), destination, savedMessage);
    }
}
