package com.tirikk.actionmonitor.messaging;

import com.tirikk.actionmonitor.authentication.UserDto;
import com.tirikk.actionmonitor.authentication.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.tirikk.actionmonitor.Constants.INDEX_TEMPLATE;
import static com.tirikk.actionmonitor.Constants.MESSAGING_TEMPLATE;
import static com.tirikk.actionmonitor.Constants.ROOT_ENDPOINT;
import static com.tirikk.actionmonitor.Constants.SENDMESSAGE_ENDPOINT;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MessagingController {
    private static final String MESSAGES_ATTR = "messages";
    private static final String USERS_ATTR = "users";
    private static final String CURRENT_USER_ATTR = "currentUser";

    private final UserService userService;

    private final MessageService messageService;

    @GetMapping(ROOT_ENDPOINT)
    public String listContacts(Model model, Principal principal) {
        List<String> userNames = userService.listUsers().stream()
                .map(UserDto::getUserName)
                .collect(toList());
        model.addAttribute(USERS_ATTR, userNames);
        model.addAttribute(CURRENT_USER_ATTR, principal.getName());
        return INDEX_TEMPLATE;
    }

    @GetMapping("/{contact}")
    public String showMessageHistory(Model model, @PathVariable String contact, Principal principal) {
        if (!userService.userExists(contact)) {
            return format("redirect:%s?error", ROOT_ENDPOINT);
        }
        List<Message> messages = messageService.listMessagesBetween(principal.getName(), contact);
        model.addAttribute(MESSAGES_ATTR, messages);
        model.addAttribute(CURRENT_USER_ATTR, principal.getName());
        return MESSAGING_TEMPLATE;
    }

    @MessageMapping(SENDMESSAGE_ENDPOINT)
    public void sendMessage(@Valid Message message, Principal principal) {
        log.info("Message received from {}", principal.getName());
        log.debug("Message target: {}, content: {}", message.getToUser(), message.getContent());
        message.setFromUser(principal.getName());
        messageService.persistAndSendMessage(message);
    }
}
