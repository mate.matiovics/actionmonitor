package com.tirikk.actionmonitor.messaging;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    @Query("from Message M where M.fromUser in (:user1, :user2) and M.toUser in (:user1, :user2) order by M.postedAt asc")
    List<Message> findAllBetweenUsers(@Param("user1") String user1, @Param("user2") String user2);
}
