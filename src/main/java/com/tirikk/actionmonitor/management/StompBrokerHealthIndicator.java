package com.tirikk.actionmonitor.management;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.messaging.simp.broker.AbstractBrokerMessageHandler;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StompBrokerHealthIndicator implements HealthIndicator {

    private final AbstractBrokerMessageHandler stompBrokerRelayMessageHandler;

    @Override public Health health() {
        boolean brokerAvailable = stompBrokerRelayMessageHandler.isBrokerAvailable();
        if (!brokerAvailable) {
            return Health.down().build();
        }
        return Health.up().build();
    }
}
