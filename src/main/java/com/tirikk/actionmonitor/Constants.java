package com.tirikk.actionmonitor;

public class Constants {
    public static final String AUTH_PATH = "/auth";
    public static final String ERROR_PATH = "/error";
    public static final String ACTUATOR_PATH = "/actuator";
    public static final String LOGIN_ENDPOINT = "/login";
    public static final String LOGOUT_ENDPOINT = "/logout";
    public static final String REGISTER_ENDPOINT = "/register";
    public static final String ROOT_ENDPOINT = "/";
    public static final String SENDMESSAGE_ENDPOINT = "/sendMessage";
    public static final String FULL_LOGIN_PATH = AUTH_PATH + LOGIN_ENDPOINT;
    public static final String FULL_LOGOUT_PATH = AUTH_PATH + LOGOUT_ENDPOINT;
    public static final String FULL_REGISTER_PATH = AUTH_PATH + REGISTER_ENDPOINT;

    public static final String PERFORM_LOGIN = "/perform_login";

    public static final String INDEX_TEMPLATE = "index";
    public static final String LOGIN_TEMPLATE = "login";
    public static final String REGISTRATION_TEMPLATE = "registration";
    public static final String MESSAGING_TEMPLATE = "messaging";

    public static final String TOPIC_PREFIX = "/topic/";
    public static final String QUEUE_PREFIX = "/queue/";

    public static final String USER_ROLE = "ROLE_USER";
}
