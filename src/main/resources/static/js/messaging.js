let contact = window.location.pathname.substring(1);
const socket = new SockJS("/action-monitor-websocket");
const stompClient = Stomp.over(socket);

stompClient.connect({}, function (frame) {
    onConnect();
});

$("#send").submit(function (e) {
    e.preventDefault();
    sendMessage();
});

function onConnect() {
    stompClient.subscribe("/user/queue/message", function (message) {
        onMessage(message);
    });
}

function onMessage(message) {
    let parsedMessage = JSON.parse(message.body);
    let refreshable = $("#toRefresh");
    let currentUser = refreshable.data("currentuser");
    if (parsedMessage.fromUser === contact || (parsedMessage.toUser === contact && parsedMessage.fromUser === currentUser)) {
        refreshable.append("<ol class='list'><li>" + parsedMessage.fromUser + ": " + parsedMessage.content + "</li></ol>");
    }
}

function sendMessage() {
    let content = $("#content").val();
    let message = {"content": content, "toUser": contact};

    stompClient.send("/app/sendMessage", {}, JSON.stringify(message));
}

window.onbeforeunload = function () {
    socket.onclose = function () {};
    socket.close();
}
