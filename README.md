<h1>"Action-monitor" application</h1>
<ul>
<li><a href="#summary">Summary</a></li>
<li><a href="#requirements">Requirements</a></li>
<li><a href="#build-and-deployment">Build and deployment</a></li>
<li><a href="#testing">Testing</a></li>
<li><a href="#how-to-exercise-the-system">How to exercise the system</a></li>
<li><a href="#additional-information">Additional information</a></li>
</ul>

<h2>Summary</h2>
<p>
This SpringBoot web application is designed to allow users to communicate with each other by pushing messages to the target.
The users must be registered and authenticated in order to be able to choose who they want to send messages to. <br/>
Messages are stored in a SQL database, and displayed as a message history between the currently logged in user
and their selected contact.
</p>
<p>
Messaging is done via WebSockets and the use of an external STOMP message broker.
Users are establishing connections and subscriptions with the broker through the application as a relay.
Users send their messages to the application, which in turn persists and forwards them to the broker, which will
push the messages to the target user. 
</p>
<p>
An overview of the design can be seen on the following diagram: <br/>
<img src="./system-design.PNG" alt="System Design Diagram">
</p>
<p>
The source code of the application is packaged by feature, and the functionality is separated into controller, service and data layers.
</p>

<h2>Requirements</h2>
<p>
To build and run the application JDK 11 and Maven 3 is necessary. Additionally to run the external components
(db, message broker, etc) Docker needs to be installed.
</p>

<h2>Build and deployment</h2>
<p>
The application can be built by executing the following command from the command line in the root folder of the project:
</p>
<pre>mvn clean package -DskipTests</pre>
<p>
From the created artifact a docker image can be built with the following command (executed in the root folder, where the Dockerfile is present):  
</p>
<pre>docker build -t actionmonitor .</pre>
<p>A docker image will be created with the name <i>actionmonitor</i>.</p>

<h4>Deployment</h4>
<p>
To deploy the application to a cloud environment a CI/CD pipeline or automation server could be configured to build and publish the artifacts,
create the docker image and push it to a docker image repository. <br/>
An IaC provisioning/management tool (e.g Terraform) could be used to provision and configure the external resources
and deploy the application's docker image to the cloud. These steps could be implemented as part of a CI/CD pipeline as well. <br/>
The implementation of deployment steps is not in the scope of this project. 
</p>

<h2>Testing</h2>
<p>
The automated unit tests can be run with the following command, which will also generate a html test report file
<i>{project}/target/site/surefire-report.html</i>:
</p>
<pre>mvn surefire-report:report</pre>
<p>
Alternatively there is a CI/CD pipeline job for running unit tests, which also generates artifacts from the test reports that are inspected upon
submitting merge requests. (This can be observed in the currently open merge request.)
</p>
<p>
The unit testing strategy used is mainly mockist, especially when unit testing the controller layer, as the mvc test framework provided by Spring
favors testing behaviour. However, in some cases assertions for object state were written as well, which is a characteristic of the classical testing strategy.
</p>

<h2>How to exercise the system</h2>
<p>
External components can be run in a virtualized environment by executing the following command in the docker/config folder of the project:
</p>
<pre>docker-compose up --build</pre>
<p>
This will run separate docker containers for a MySQL database (on localhost:3306), ActiveMQ Artemis (open to WebSocket connections on localhost:61613)
and PHPMyAdmin for db management with the following details: 
</p>
<table>
<tr>
<th>Component</th>
<th>Host</th>
<th>Port</th>
<th>Username</th>
<th>Password</th>
</tr>
<tr>
<td>Artemis Web Interface</td>
<td>localhost</td>
<td>8161</td>
<td>artemis</td>
<td>password</td>
</tr>
<tr>
<td>PHPMyAdmin</td>
<td>localhost</td>
<td>15673</td>
<td>root</td>
<td>password</td>
</tr>
</table>
<p>
Once these components are up and running, the application can be started in a separate container (connected to the same docker network)
by executing the same command in the docker subfolder of the project. <br/>
The properties and environment variables of the various components can be customized in their respective docker-compose.yml files.
</p>
<p>
The DB is preconfigured to contain two registered users with the names <i>Gilfoyle</i> and <i>Erlich</i>, for both of which the password is
 <i>Password1234</i>.<br/>
After calling the application on port 8080 of localhost, these users can be used to log in to the system and manually test the messaging functionality.
(Naturally new users can be registered as well.)
</p>

<h2>Additional information</h2>
<p>
The status and build information of the application can be accessed through the <i>/actuator/health</i> and <i>/actuator/info</i> endpoints respectively.
</p>
<p>
With the introduction of externalized session state using Spring Session and a storage like Redis the application could be made horizontally scalable, but this
is outside of the scope of this project. 
</p>
<p>
<i>Note: The styling on the pages of the application is minimal (and ugly) as the emphasis was on the backend functionality.</i>
</p>
